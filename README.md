Procedimentos para execução da aplicação:

1. Utilizar Servidor de aplicação Apache Tomcat 7.0.x;
2. Utilizar Banco de dados MySQL;

IMPORTANTE:

- Antes de efetuar o deploy da aplicação, criar um banco chamado "javawebfinal"e alterar no arquivo persistence.xml as informações para conexão com a base;
- Executar os comandos contidos no arquivo "comandos_trab_final_java_web.txt", presente na pasta raíz do projeto.

Abraço.