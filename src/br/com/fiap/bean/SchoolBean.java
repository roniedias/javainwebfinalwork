package br.com.fiap.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import br.com.fiap.dao.GenericDao;
import br.com.fiap.entity.School;

@ManagedBean(name= "schoolBean" )
@RequestScoped
public class SchoolBean implements Serializable {
	
	private static final long serialVersionUID = -2120502773259448980L;

	private School school;
	
	public SchoolBean(){
		school = new School();
	}
	
	public School getSchool() {
		return school;
	}
	public void setSchool(School school) {
		this.school = school;
	}

	public String addSchool(){
		try {
			GenericDao<School> dao = new GenericDao<School>(School.class);
			dao.add(school);
			
			return "/success"; //dispatching para sucesso.xhtml
		} catch (Exception e) {
			return "/error"; //dispatching para erro.xhtml
		}
	}
	
	public List<School> listSchools(){
		GenericDao<School> dao = new GenericDao<School>(School.class);
		return dao.list();
	}
}
