package br.com.fiap.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import br.com.fiap.dao.GenericDao;
import br.com.fiap.entity.Course;
import br.com.fiap.entity.Role;
import br.com.fiap.entity.Student;
import br.com.fiap.entity.User;

@ManagedBean(name = "studentBean" )
@RequestScoped
public class StudentBean implements Serializable  {
		
	private static final long serialVersionUID = 7214901554586424385L;
	
	private Student student;
	
	public StudentBean(){
		student = new Student();
	}
	
	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}

	public String addStudent(String courseName){
		try {
			GenericDao<Student> dao = new GenericDao<Student>(Student.class);
			GenericDao<Course> courseDao = new GenericDao<Course>(Course.class);

			Role role = new Role();
			role.setRoleId(2);
			student.setRole(role);
			
			Course course = courseDao.findByName(courseName);
			student.setCourse(course);
					
			dao.add(student);
			return "/success"; 
		} catch (Exception e) {
			e.printStackTrace();
			return "/error"; 
		}
	}
	
	public List<Student> listStudents(){
		GenericDao<Student> dao = new GenericDao<Student>(Student.class);
		return dao.list();
	}
	
	public List<Student> listStudentsByCourseId(int courseId) {
		List<Student> students = listStudents();
		List<Student> studentsByCourseId = new ArrayList<Student>();
		for(Student s : students) {
			if(s.getCourse().getCourseId() == courseId) {
				studentsByCourseId.add(s);
			}
		}
		return studentsByCourseId;
	}
	
	public Student studentByName(String name) {
		GenericDao<Student> dao = new GenericDao<Student>(Student.class);
		Student student = dao.findByName(name);
		return student;
	}
	
	public int studentIdByStudentName(String studentName) {
		GenericDao<Student> dao = new GenericDao<Student>(Student.class);
		Student student = dao.findByName(studentName);
		return student.getStudentId();
	}
	
}
