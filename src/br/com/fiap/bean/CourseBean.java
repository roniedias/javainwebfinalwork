package br.com.fiap.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import br.com.fiap.dao.GenericDao;
import br.com.fiap.entity.Course;

@ManagedBean(name = "courseBean" )
@RequestScoped
public class CourseBean implements Serializable {
	
	private static final long serialVersionUID = -2559732822190380389L;
	
	private Course course;
	
	public CourseBean(){
		course = new Course();
	}
	
	public Course getCourse() {
		return course;
	}
	public void setCourse(Course course) {
		this.course = course;
	}

	public String addCourse(){
		try {
			GenericDao<Course> dao = new GenericDao<Course>(Course.class);
			dao.add(course);
			
			return "/success"; 
		} catch (Exception e) {
			return "/error"; 
		}
	}
	
	public List<Course> listCourses(){
		GenericDao<Course> dao = new GenericDao<Course>(Course.class);
		return dao.list();
	}
	
	public Course courseByName(String name) {
		GenericDao<Course> dao = new GenericDao<Course>(Course.class);
		Course course = dao.findByName(name);
		return course;
	}
	
	public Course courseById(int id) {
		GenericDao<Course> dao = new GenericDao<Course>(Course.class);
		return dao.find(id);
	}
	
	public List<String> studentNamesByCourseId(String courseId) {		
		String query = "SELECT s.name FROM student s inner join course c on s.courseId = c.courseId WHERE c.courseId = '" + courseId + "'";
		GenericDao<String> dao = new GenericDao<String>(String.class);
		List<String> studentNames = dao.nativeQuery(query);
		return studentNames;
	}
	
	public List<String> professorNamesByCourseId(String courseId) {		
		String query = "SELECT p.name FROM professor p INNER JOIN discipline d on p.professorId = d.professorId WHERE d.courseId = '" + courseId + "'";
		GenericDao<String> dao = new GenericDao<String>(String.class);
		List<String> studentNames = dao.nativeQuery(query);
		return studentNames;
	}

}
