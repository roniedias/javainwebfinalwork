package br.com.fiap.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import br.com.fiap.dao.GenericDao;
import br.com.fiap.entity.Discipline;
import br.com.fiap.entity.Professor;
import br.com.fiap.entity.Role;

@ManagedBean(name = "professorBean" )
@RequestScoped
public class ProfessorBean implements Serializable {
	
	private static final long serialVersionUID = -2559732822190380389L;
	
	private Professor professor;
	
	public ProfessorBean(){
		professor = new Professor();
	}
	
	public Professor getProfessor() {
		return professor;
	}
	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public String addProfessor(){
		try {
			GenericDao<Professor> dao = new GenericDao<Professor>(Professor.class);
			
			Role role = new Role();
			role.setRoleId(3);
			professor.setRole(role);
			
			dao.add(professor);
			
			return "/success"; 
		} catch (Exception e) {
			e.printStackTrace();
			return "/error"; 
		}
	}
	
	public List<Professor> listProfessors(){
		GenericDao<Professor> dao = new GenericDao<Professor>(Professor.class);
		return dao.list();
	}
	
	public List<Discipline>disciplinesByProfessorName(String professorName) {
		List<Professor> professors = listProfessors();
		List<Discipline> disciplines = new ArrayList<Discipline>();
		for(Professor p : professors) {
			if(p.getName().toLowerCase().trim().equals(professorName.toLowerCase().trim())) {
				disciplines.addAll(p.getDisciplines());
			}
		}
		return disciplines;
	}
	
	public List<String> studentNamesByProfessorName(String professorName) {
		String query = "SELECT DISTINCT query1.studentName FROM (SELECT s.name as studentName, d.disciplineId FROM student s inner join discipline d ON s.courseId = d.courseId) query1 JOIN (SELECT p.name as professorName, d.disciplineId from professor p inner join discipline d ON p.professorId = d.professorId where p.name = '" + professorName + "') query2 WHERE query1.disciplineId = query2.disciplineId";
		GenericDao<String> dao = new GenericDao<String>(String.class);
		List<String> studentNames = dao.nativeQuery(query);
		return studentNames;
	}
	
}
