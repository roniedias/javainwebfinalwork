package br.com.fiap.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import br.com.fiap.dao.GenericDao;
import br.com.fiap.entity.Role;

@ManagedBean(name = "roleBean" )
@RequestScoped
public class RoleBean implements Serializable {
	
	private static final long serialVersionUID = -2202427457483800918L;

	private Role role;
	
	public RoleBean(){
		role = new Role();
	}
	
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}

	public String addRole(){
		try {
			GenericDao<Role> dao = new GenericDao<Role>(Role.class);
			dao.add(role);
			
			return "/success"; 
		} catch (Exception e) {
			return "/error"; 
		}
	}
	
	public List<Role> listRoles(){
		GenericDao<Role> dao = new GenericDao<Role>(Role.class);
		return dao.list();
	}
		
}
