package br.com.fiap.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.com.fiap.dao.GenericDao;
import br.com.fiap.dao.UserDao;
import br.com.fiap.entity.Professor;
import br.com.fiap.entity.User;

@ManagedBean(name = "loginBean" )
@SessionScoped
public class LoginBean implements Serializable {
	
	private static final long serialVersionUID = -2559732822190380389L;
	
	private User user;
	
	private List<String> studentNameByProfessorList;
	
	public LoginBean(){
		user = new User();
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public String login() {
		UserDao userDao = new UserDao(User.class);
		user = userDao.login(user);
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", user);
		
		if(user == null){
			return "/no_access.xhtml";
		}else{
			if(user.getRole().getName().equals("Professor")){
				studentNameByProfessorList = loadStudentNameByProfessorList();
			}
			return "/index.xhtml";
		}
	}
	
	public List<String> loadStudentNameByProfessorList() {		
		GenericDao<Professor> dao = new GenericDao<Professor>(Professor.class);
		Professor professor = dao.find(user.getStudentProfessorId());
		List<String> studentNamesByProfessorName = new ProfessorBean().studentNamesByProfessorName(professor.getName());
		return studentNamesByProfessorName;
	}

	public List<String> getStudentNameByProfessorList() {
		return studentNameByProfessorList;
	}

	public void setStudentNameByProfessorList(List<String> studentNameByProfessorList) {
		this.studentNameByProfessorList = studentNameByProfessorList;
	}
	
	public boolean isMyStudent(String studentName){
		return studentNameByProfessorList.contains(studentName);
	}
	
	public String logout(){
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "login";
	}
	
	
}
