package br.com.fiap.dao;

import java.util.List;

public interface Dao<T> {
	void add(T entity);
	 List<T> list();
	 T find(int id);
	T findByName(String name);
	List<T> nativeQuery(String query);
	T nativeQueryObject(String query); 
}
