package br.com.fiap.dao;

import java.util.List;

import javax.persistence.EntityManager;

public class GenericDao<T> implements Dao<T> {

	private final Class<T> clazz;
	protected EntityManager em;

	public GenericDao(Class<T> classe) {
		this.clazz = classe;
	}

	@Override
	public void add(T entidade) {
		em = JpaUtil.getEntityManager();
		em.getTransaction().begin();
		em.persist(entidade);
		em.getTransaction().commit();
		em.close();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> list() {
		em = JpaUtil.getEntityManager();
//		return em.createQuery("SELECT c From " + clazz.getSimpleName() + " c").getResultList();
		List<T> list = em.createQuery("SELECT c From " + clazz.getSimpleName() + " c").getResultList();
		em.close();
		return list;
	}

	@Override
	public T find(int id) {
		em = JpaUtil.getEntityManager();
		em.getTransaction().begin();
		T entity = em.find(clazz, id);
		em.getTransaction().commit();
		em.close();
		return entity;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public T findByName(String name) {
		em = JpaUtil.getEntityManager();
		List<T> list = em.createQuery("SELECT c From " + clazz.getSimpleName() + " c WHERE c.name = '" + name + "'").getResultList();
		em.close();
		T entity;
		if(list.size() > 0) {
			entity = list.iterator().next();
			return entity;
		}
		return null;		
	}
	
	
	// *** Native queries ***
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> nativeQuery(String query) {
		em = JpaUtil.getEntityManager();
		List<T> list = em.createNativeQuery(query).getResultList();
		em.close();
		return list;
	}
	
}
