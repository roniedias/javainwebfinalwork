package br.com.fiap.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "grade")
public class Grade {
	
	@Id
	@Column(name = "gradeId")
	private int gradeId;
	
	@Column(name = "grade1", length = 100)
	private double grade1;
	
	@Column(name = "grade2", length = 100)
	private double grade2;

	@Column(name = "grade3", length = 100)
	private double grade3;

	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="studentId")
	private Student student = new Student();
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="disciplineId")
	private Discipline discipline = new Discipline();
	
	@Column(name = "status", length = 100)
	private String status;

	public int getGradeId() {
		return gradeId;
	}
	public void setGradeId(int gradeId) {
		this.gradeId = gradeId;
	}

	public double getGrade1() {
		return grade1;
	}
	public void setGrade1(double grade1) {
		this.grade1 = grade1;
	}

	public double getGrade2() {
		return grade2;
	}
	public void setGrade2(double grade2) {
		this.grade2 = grade2;
	}
	
	public double getGrade3() {
		return grade3;
	}
	public void setGrade3(double grade3) {
		this.grade3 = grade3;
	}
	
	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}
	
	public Discipline getDiscipline() {
		return discipline;
	}
	public void setDiscipline(Discipline discipline) {
		this.discipline = discipline;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}