package br.com.fiap.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "student")
public class Student {

	@Id
	@Column(name = "studentId")
	private int studentId;
	
	@Column(name = "name", length = 100)
	private String name;
	
	@ManyToOne(fetch = FetchType.EAGER) @JoinColumn(name = "courseId") 
	private Course course;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="roleId")
	private Role role;

	
	public int getStudentId() {
		return studentId;
	}
	public void setId(int studentId) {
		this.studentId = studentId;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Course getCourse() {
		return course;
	}
	public void setCourse(Course course) {
		this.course = course;
	}
	
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
		
}
